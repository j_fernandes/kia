// Search
// Click heading to show elements
(function($) {
	$('#side_panel.search--closed').click( function() {
		var $this = $( this );
			if ( $this.hasClass( "search--open") ) {
				return true;
			}
		$this.removeClass("search--closed");
		$this.addClass( "search--open");
		$('.search__element').toggleClass('is-visible is-hidden');
		$('.matrix').toggleClass("search-is-open search-is-closed");
	});

	$('.search__close-icon').click(function() {
		$('.search').removeClass( "search--open").trigger( "refresh" );
		$('.search').addClass( "search--closed" ).trigger( "refresh" );

		$('.search__element').toggleClass('is-visible is-hidden');
		$('.matrix').toggleClass("search-is-open search-is-closed");
		return false;

	});
	var allPanels = $('.search--closed .search__element > dd').hide();
	$('.search--closed .search__element > dt > span').click(function() {
		$this = $(this);
		$target = $this.parent().next();
		if(!$target.hasClass('active')){
			allPanels.removeClass('active').slideUp();
			$target.addClass('active').slideDown();
		}

		var spans = $('.search--open .search__element > dt > span');
		spans.each( function( index ) {
			if ( $( this ) === $this ) {
				return false;
			} else {
				$( this ).removeClass( "active" );
			}
		});

		$this.addClass('active');
		return false;
	});
})(jQuery);
$('.checkbox').on('click', function () {
	console.log("test");
	$(this).toggleClass('checked');
	var $checkbox = $('input').next(':checkbox');
	$checkbox.attr('checked', !$checkbox.attr('checked'));
});


// Matrix
// Checks check-box when clicking on the image
$('.model').on('click', function () {
	$(this).toggleClass('checked');
	var $checkbox = $('.model__image').next(':checkbox');
	$checkbox.attr('checked', !$checkbox.attr('checked'));
});
$('.model__optionslink').click( function() {
	$('.tabs .is-disabled').toggleClass('active');
});

// Vehicle Details
// Gallery
$(window).load(function() {
	// The slider being synced must be initialized first
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: true,
		slideshow: true,
		itemWidth: 210,
		itemMargin: 5,
		pauseOnHover: true,
		touch: true,
		asNavFor: '#slider'
	});
	$('#slider').flexslider({
		animation: "slide",
		controlNav: false,
		directionNav: false,
		animationLoop: true,
		slideshow: true,
		pausePlay: true,
		touch: true,
		pauseText: "",
		playText: "",
		sync: "#carousel"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		itemWidth: 210,
		itemMargin: 5,
		minItems: 2,
		maxItems: 5
	});
});




// Vehicle Details
// Contact form

$(document).ready(function() {
	$('.contact').magnificPopup({
		type: 'ajax',
		alignTop: true,
		overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
	});

	$('.ajax-popup').magnificPopup({
		type: 'ajax'
	});
	$('.print-details').magnificPopup({
		type: 'iframe'
	});
});

// Model Info pages
$(document).pjax('.model__optionslink', '.tab_content');