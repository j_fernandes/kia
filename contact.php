<section class="contact-form">
	<h1 class="main__heading">
		Email Us
		<!-- {translate}Email Us{/translate} -->
	</h1>
	<h2 class="sub__heading">
		KIA 2012 Sportage 1.6 GDI Spirit / &euro;23.590 / 1,000km
	</h2>
	<p>Please fill in your details (fields maker with <span class="required">*</span> are mandatory)
	<form class="cf">
		<div class="col full / interest">
			<fieldset>
				<p class="float-left">I am interested in <span class="required">*</span></p>
				<div class="checkbox">
					<input type="checkbox" id="additional-info" value="additional-info" name="additional-info">
					<label for="additional-info"><span for="additional-info"></span> Additional Information</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" id="test-drive" value="test-drive" name="test-drive">
					<label for="test-drive"><span for="test-drive"></span> Test Drive</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" id="finance-quote" value="finance-quote" name="finance-quote">
					<label for="finance-quote"><span for="finance-quote"></span> Finance Quote</label>
				</div>
			</fieldset>
		</div>
		<div class="col half">
			<fieldset>
				<legend>Personal Details</legend>
				<!-- <legend>{translate}Personal Details{/translate}</legend> -->
				<label for="title">Title <span class="required">*</span></label><input type="text" name="title" id="title">
				<label for="firstname">First Name <span class="required">*</span></label><input type="text" name="firstname" id="firstname">
				<label for="lastname">Last Name <span class="required">*</span></label><input type="text" name="lastname" id="lastname">
				<legend>Method of contact</legend>
				<!-- <legend>{translate}Method of contact{/translate}</legend> -->
				<label for="phone">Phone <span class="required">*</span></label><input type="tel" name="phone" id="phone">
				<label for="email">Email <span class="required">*</span></label><input type="email" name="email" id="email">
			</fieldset>
		</div><!-- /.col .half -->
		<div class="col half">
			<fieldset>
				<legend>Address</legend>
				<!-- <legend>{translate}Address{/translate}</legend> -->
				<label for="postcode">Postcode <span class="required">*</span></label><input type="text" class="postcode" name="postcode" id="postcode">
				<input type="button" value="Lookup" onclick="checkpostcode();" class="lookup button button--large button--red">
				<label for="address">Choose Address</label>
				<select name="address_options" class="input" onchange="getaddress();">
					<option>Enter postcode above</option>
					<option value="1">219</option>
					<option value="2">221</option>
					<option value="3">223</option>
				</select>
				<label for="address2">Address Line 2</label><input type="text" class="input readonly" id="address2" name="address2">
				<label for="address3">Address Line 3</label><input type="text" class="input readonly" id="address3" name="address3">
                <label for="address4">Address Line 4</label><input type="text" class="input readonly" id="address4" name="address4">
			</fieldset>
		</div><!-- /.col .half -->
		<div class="col full">
			<fieldset>
				<legend>Comments</legend>
				<textarea name="comments" id="comments" cols="30" rows="5"></textarea>
			</fieldset>
		</div><!-- /.col full -->
		<div class="col full / terms">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam explicabo alias ea minima, aperiam laborum omnis quasi optio voluptatibus saepe reprehenderit veniam consectetur, a consequatur quo quas! Velit, itaque perspiciatis!</p>
		</div><!-- /.col full -->
		<div class="col full">
			<input type="submit" value="Submit" class="button button--large button--red float-right">
		</div><!-- /.col full -->
	</form>
</section><!-- /.contact-form -->