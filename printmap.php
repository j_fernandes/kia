<!DOCTYPE html>
<!--
// ////////////////////////////////////////////////////////////////////////////
//
// Developed by Motortrak Ltd
//
// Address:   Motortrak Ltd
//            AC Court
//            High Street
//            Thames Ditton
//            KT7 0SR
//
// Software:  my.Motortrak
//
// Tel:       +44 (0)20 8335 2000
// E-Mail:    info@motortrak.com
//
// Copyright: Motortrak Ltd {'Y'|date}. All rights reserved.
//
// ////////////////////////////////////////////////////////////////////////////
-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <title>Kia</title>

        <meta name="application-name" content="{$application->applicationBuildMeta}"/>
        <meta name="keywords" content="{$meta->keywords}">
        <meta name="description" content="{$meta->description}">

        <meta http-equiv="cleartype" content="on">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0" />

        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="stylesheet" href="/assets/css/base.css">
        <link rel="stylesheet" href="/assets/css/kia.css">
        <script src="/assets/js/modernizr.min.js"></script>
    {literal}
        <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script>
            var directionDisplay;
            var directionsService = new google.maps.DirectionsService();
            var map;

            function initialize() {
                directionsDisplay = new google.maps.DirectionsRenderer();
            var image = '';
        {/literal}
            var centermap = new google.maps.LatLng({$lat|strip_tags}, {$long|strip_tags}) ;
        {literal}
            var myOptions = {
                zoom:10,
                mapTypeControl: true,
                mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                navigationControl: true,
                navigationControlOptions: {
                style: google.maps.NavigationControlStyle.ZOOM_PAN,
                position: google.maps.ControlPosition.TOP_LEFT},
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: centermap
            }
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                directionsDisplay.setMap(map);
                directionsDisplay.setPanel(document.getElementById("directionsPanel"));

            var marker = new google.maps.Marker({
                position: centermap,
                map: map,
                icon: image
            });
        }

            function calcRoute() {
            var start = document.getElementById("start").value;);
        {/literal}
            var lat = parseFloat('{$lat|strip_tags}');
            var lng = parseFloat('{$long|strip_tags}');
            var end = new google.maps.LatLng(lat, lng);
        {literal}
            var request = {
                origin:start,
                destination:end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
        		    unitSystem: google.maps.UnitSystem.IMPERIAL
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });
        }
        </script>
        {/literal}
    </head>
    <body onload="initialize();calcRoute();" id="Form" class="kia print">
        {assign var=addressFound value=false}
        {foreach from=$dealer->departmentAddresses key=dept item=address name="addresses"}
        {if $smarty.foreach.addresses.first}
            <input type="hidden" id="end" value="{$address->latitude},{$address->longitude}" />
        {/if}
        {/foreach}

        {php}
        //security fix for postcode
        $startValue = urldecode($this->get_template_vars('start'));
        $pattern = "|^[0-9a-zA-Z ]+$|";
        $startValue = preg_grep($pattern, array($startValue));
        $startValue = implode("", $startValue);
        {/php}

        <input type="hidden" id="start" value="{php}echo $startValue;{/php}"/><input type="hidden" id="end" value="{$end}">
        <input name="directions" type="hidden" value="{php}echo $startValue;{/php}" onClick="this.value=''" id="start">
        <header class="container / cf">
          <div class="content">
            <div class="header--logo">
              <a href="/" title="Kia">
                <img src="/assets/imgs/kia-logo.png" alt="Kia">
              </a>
            </div>
            <h1 class="header--heading">KIA Used Cars - Dealername</h1>
            <!-- <h1 class="header--heading">KIA Used Cars - {$items.DEALERNAME}</h1> -->
          </div><!-- /.content -->
        </header>
        <div class="container">
            <div class="content">
                <button class="float-right / button button--grey print" onclick="window.print()">Print</button>
                <div id="map_canvas"></div>
                <div id="directionsPanel"></div>
            </div><!-- /.content -->
        </div><!-- /.container -->
    </body>
</html>