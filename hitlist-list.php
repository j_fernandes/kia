<!DOCTYPE html>
<!--
// ////////////////////////////////////////////////////////////////////////////
//
// Developed by Motortrak Ltd
//
// Address:   Motortrak Ltd
//            AC Court
//            High Street
//            Thames Ditton
//            KT7 0SR
//
// Software:  my.Motortrak
//
// Tel:       +44 (0)20 8335 2000
// E-Mail:    info@motortrak.com
//
// Copyright: Motortrak Ltd {'Y'|date}. All rights reserved.
//
// ////////////////////////////////////////////////////////////////////////////
-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no">
		<title>Kia</title>

		<meta name="application-name" content="{$application->applicationBuildMeta}"/>
        <meta name="keywords" content="{$meta->keywords}">
        <meta name="description" content="{$meta->description}">

        <meta http-equiv="cleartype" content="on">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0" />

		<link rel="shortcut icon" href="{$domainname_uri}/tomhartleyjnr/img/favicon.ico" />

		 <link rel="stylesheet" href="/assets/css/base.css">
		 <link rel="stylesheet" href="/assets/css/kia.css">
		 <link rel="stylesheet" href="/assets/css/magic.min.css">

	</head>
	<body class="kia">
		<header class="container / cf">
			<div class="content">
				<div class="header--logo">
					<a href="/" title="Kia">
						<img src="/assets/imgs/kia-logo.png" alt="Kia">
					</a>
				</div>
				<h1 class="header--heading">Kia Used Cars - Region</h1>
				<nav class="nav--main">
					<ul>
						<li><a href="#" class="active" title="Search">Search</a></li>
						<li><a href="#" title="Kia Used Cars">Kia Used Cars</a></li>
					</ul>
				</nav>
			</div><!-- /.content -->
		</header>
		<div class="container / hitlist">
			<form>
				<div class="cf / content">
					<main class="cf">
						<div class="search / search--open">
							<div class="cf / search__header">
								<i class="icon / search__search-icon"></i>
								<h3 class="search__title">Advanced</h3>
							</div><!-- /.search .search--open -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title first">Distance <i class="icon arrow"></i></span>
								</dt>
								<dd class="search__element__distance">
									<input type="text" id="postcode" name="postcode">
									<select id="distance" name="distance">
										<option>Any Distance</option>
										<option value="10">10 miles</option>
										<option value="20">20 miles</option>
										<option value="30">30 miles</option>
									</select>
								</dd><!-- /.search__element__distance -->
							</dl><!-- /.search__element -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title">Mileage <i class="icon arrow"></i></span>
								</dt>
								<dd class="search__element__mileage">
									<select id="mileage" name="mileage">
										<option>Any (up to)</option>
										<option value="20000">Up to 20,000 km</option>
										<option value="40000">Up to 40,000 km</option>
										<option value="60000">Up to 60,000 km</option>
										<option value="80000">Up to 80,000 km</option>
										<option value="100000">Up to 100,000 km</option>
										<option value="120000">Up to 120,000 km</option>
									</select>
								</dd><!-- /.search__element__mileage -->
							</dl><!-- /.search__element -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title">Age <i class="icon arrow"></i></span>
								</dt>
								<dd class="cf / search__element__age">
									<select id="agefrom" name="age">
										<option>From</option>
									</select>
									<select id="ageto" name="age">
										<option>To</option>
									</select>
								</dd><!-- /.search__element__age -->
							</dl><!-- /.search__element -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title">Bodystyle <i class="icon arrow"></span></i>
								</dt>
								<dd class="cf / search__element__bodystyle">
									<div class="checkbox">
										<div class="left">
											<input type="checkbox" id="hatchback" name="hatchback" />
											<label for="hatchback"><span for="hatchback"></span>Hatchback</label>
											<input type="checkbox" id="estate" name="estate" />
											<label for="estate"><span for="estate"></span>Estate</label>
											<input type="checkbox" id="suv" name="suv" />
											<label for="suv"><span for="mpv"></span>SUV</label>
										</div><!-- /.left -->
										<div class="right">
											<input type="checkbox" id="saloon" name="saloon" />
											<label for="saloon"><span for="saloon"></span>Saloon</label>
											<input type="checkbox" id="mpv" name="mpv" />
											<label for="mpv"><span for="mpv"></span>MPV</label>
										</div><!-- /.right -->
									</div><!-- /.checkbox -->
								</dd><!-- /.search__element__bodystyle -->
							</dl><!-- /.search__element -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title">Transmission <i class="icon arrow"></span></i>
								</dt>
								<dd class="search__element__transmission">
									<select id="transmission" name="transmission">
										<option value="">Any</option>
										<option value="">Manual</option>
										<option value="">Automatic</option>
									</select>
								</dd><!-- /.search__element__transmission -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-open">
								<dt>
									<span class="search__element__title">Fuel Type <i class="icon arrow"></span></i>
								</dt>
								<dd class="search__element__transmission">
									<select id="transmission" name="transmission">
										<option value="">Any</option>
										<option value="">Petrol</option>
										<option value="">Diesel</option>
									</select>
								</dd><!-- /.search__element__transmission -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-open">
								<dt>
									<span class="search__element__title">Fuel Type <i class="icon arrow"></span></i>
								</dt>
								<dd class="search__element__transmission">
									<select id="transmission" name="transmission">
										<option value="">Any</option>
										<option value="">Petrol</option>
										<option value="">Diesel</option>
									</select>
								</dd><!-- /.search__element__transmission -->
							</dl><!-- /.search__element -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title">Colour <i class="icon arrow"></span></i>
								</dt>
								<dd class="cf / search__element__colour">
									<div class="checkbox">
										<div class="black">
											<input type="checkbox" id="black" value="black" name="black">
											<label for="black"><span for="black"></span></label>
										</div>
										<div class="white">
											<input type="checkbox" id="white" value="white" name="white">
											<label for="white"><span for="white"></span></label>
										</div>
										<div class="grey">
											<input type="checkbox" id="grey" value="grey" name="grey">
											<label for="grey"><span for="grey"></span></label>
										</div>
										<div class="brown">
											<input type="checkbox" id="brown" value="brown" name="brown">
											<label for="brown"><span for="brown"></span></label>
										</div>
										<div class="blue">
											<input type="checkbox" id="blue" value="blue" name="blue">
											<label for="blue"><span for="blue"></span></label>
										</div>
										<div class="green">
											<input type="checkbox" id="green" value="green" name="green">
											<label for="green"><span for="green"></span></label>
										</div>
										<div class="red">
											<input type="checkbox" id="red" value="red" name="red">
											<label for="red"><span for="red"></span></label>
										</div>
										<div class="yellow">
											<input type="checkbox" id="yellow" value="yellow" name="yellow">
											<label for="yellow"><span for="yellow"></span></label>
										</div>
									</div><!-- /.checkbox -->
								</dd><!-- /.search__element__colour -->
							</dl><!-- /.search__element -->

							<dl class="search__element is-open">
								<dt>
									<span class="search__element__title">Price <i class="icon arrow"></i></span>
								</dt>
								<dd class="cf / search__element__price">
									<select>
										<option value="">From</option>
									</select>
									<select>
										<option value="">To</option>
									</select>
								</dd><!-- /.search__element__price -->
							</dl><!-- /.search__element -->

							<div class="search__element is-open">
								<div class="search__element__used / checkbox">
									<input type="checkbox" id="used" name="used" />
									<label for="used"><span for="used"></span>Kia Used Cars</label>
	                            </div><!-- /.search__element__used -->
							</div><!-- /.search__element -->

							<div class="search__element / search__element--fixed">
								<div class="search__element__reset">
									<i class="icon / reset"></i><input type="reset">
								</div><!-- /.search__element__reset -->
							</div><!-- /.search__element -->
						</div><!-- /.search / .search--closed -->

						<div class="toolbar">
							<div class="pagination">
								<ul>
									<li><a href="#"><i class="icon prev is-disabled"></i></a></li>
									<li><span>Page 1 of 20</span></li>
									<li><a href="#"><i class="icon next"></i></a></li>
								</ul>
							</div><!-- /.pagination -->
							<div class="float-right">
								<div class="switcher">
									<ul>
										<li><a href="#"><i class="icon list is-selected"></i></a></li>
										<li><a href="hitlist.php"><i class="icon grid"></i></a></li>
									</ul>
								</div><!-- /.pagination -->
								<div class="saved">
									<button>Saved Vehicles</button>
								</div><!-- /.saved -->
								<div class="sorter">
									<select id="agefrom" name="age">
										<option>From</option>
									</select>
									<select id="ageto" name="age">
										<option>To</option>
									</select>
								</div><!-- /.sorter -->
							</div>
						</div><!-- /.toolbar -->
						<div class="results results--list / cf">
							<div class="vehicle">
								<a href="">
									<div class="vehicle__image">
										<i class="icon / vehicle__img-count"><span>21</span></i>
										<img src="../assets/imgs/__temp/car-1.jpg" alt="">
									</div><!-- /.image -->
									<div class="vehicle_spec">
										<div class="cf / spec-row">
											<h3 class="vehicle__title">2012 Sportage EX 2.0 4WD</h3>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__colour">Charcoal grey with Black Leather</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__mileage">1,000km</span>
											<span class="vehicle__location">Niederlasung Frankfurt</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__phone">Tel: 069/2601010</span>
										</div>
									</div><!-- /.vehicle_spec -->
									<div class="vehicle__cta">
										<div class="cf / spec-row">
											<span class="vehicle__price">&euro;19,990</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__distance">1.2 km from postcode</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__save">
												<input type="checkbox" id="save" name="save" />
												<label for="save"><span for="save"></span>Save Vehicle</label>
											</span>
										</div>
										<span class="vehicle__approved">
											<img src="../assets/imgs/approved-logo.gif">
										</span>
									</div><!-- /.vehicle__cta -->
								</a>
							</div><!-- /.vehicle -->
							<div class="vehicle">
								<a href="">
									<div class="vehicle__image">
										<i class="icon / vehicle__img-count"><span>21</span></i>
										<img src="../assets/imgs/__temp/car-1.jpg" alt="">
									</div><!-- /.image -->
									<div class="vehicle_spec">
										<div class="cf / spec-row">
											<h3 class="vehicle__title">2012 Sportage EX 2.0 4WD</h3>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__colour">Charcoal grey with Black Leather</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__mileage">1,000km</span>
											<span class="vehicle__location">Niederlasung Frankfurt</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__phone">Tel: 069/2601010</span>
										</div>
									</div><!-- /.vehicle_spec -->
									<div class="vehicle__cta">
										<div class="cf / spec-row">
											<span class="vehicle__price">&euro;19,990</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__distance">1.2 km from postcode</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__save">
												<input type="checkbox" id="save" name="save" />
												<label for="save"><span for="save"></span>Save Vehicle</label>
											</span>
										</div>
									</div><!-- /.vehicle__cta -->
								</a>
							</div><!-- /.vehicle -->
							<div class="vehicle">
								<a href="">
									<div class="vehicle__image">
										<i class="icon / vehicle__img-count"><span>21</span></i>
										<img src="../assets/imgs/__temp/car-1.jpg" alt="">
									</div><!-- /.image -->
									<div class="vehicle_spec">
										<div class="cf / spec-row">
											<h3 class="vehicle__title">2012 Sportage EX 2.0 4WD</h3>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__colour">Charcoal grey with Black Leather</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__mileage">1,000km</span>
											<span class="vehicle__location">Niederlasung Frankfurt</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__phone">Tel: 069/2601010</span>
										</div>
									</div><!-- /.vehicle_spec -->
									<div class="vehicle__cta">
										<div class="cf / spec-row">
											<span class="vehicle__price">&euro;19,990</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__distance">1.2 km from postcode</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__save">
												<input type="checkbox" id="save" name="save" />
												<label for="save"><span for="save"></span>Save Vehicle</label>
											</span>
										</div>
									</div><!-- /.vehicle__cta -->
								</a>
							</div><!-- /.vehicle -->
							<div class="vehicle">
								<a href="">
									<div class="vehicle__image">
										<i class="icon / vehicle__img-count"><span>21</span></i>
										<img src="../assets/imgs/__temp/car-1.jpg" alt="">
									</div><!-- /.image -->
									<div class="vehicle_spec">
										<div class="cf / spec-row">
											<h3 class="vehicle__title">2012 Sportage EX 2.0 4WD</h3>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__colour">Charcoal grey with Black Leather</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__mileage">1,000km</span>
											<span class="vehicle__location">Niederlasung Frankfurt</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__phone">Tel: 069/2601010</span>
										</div>
									</div><!-- /.vehicle_spec -->
									<div class="vehicle__cta">
										<div class="cf / spec-row">
											<span class="vehicle__price">&euro;19,990</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__distance">1.2 km from postcode</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__save">
												<input type="checkbox" id="save" name="save" />
												<label for="save"><span for="save"></span>Save Vehicle</label>
											</span>
										</div>
									</div><!-- /.vehicle__cta -->
								</a>
							</div><!-- /.vehicle -->
							<div class="vehicle">
								<a href="">
									<div class="vehicle__image">
										<i class="icon / vehicle__img-count"><span>21</span></i>
										<img src="../assets/imgs/__temp/car-1.jpg" alt="">
									</div><!-- /.image -->
									<div class="vehicle_spec">
										<div class="cf / spec-row">
											<h3 class="vehicle__title">2012 Sportage EX 2.0 4WD</h3>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__colour">Charcoal grey with Black Leather</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__mileage">1,000km</span>
											<span class="vehicle__location">Niederlasung Frankfurt</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__phone">Tel: 069/2601010</span>
										</div>
									</div><!-- /.vehicle_spec -->
									<div class="vehicle__cta">
										<div class="cf / spec-row">
											<span class="vehicle__price">&euro;19,990</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__distance">1.2 km from postcode</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__save">
												<input type="checkbox" id="save" name="save" />
												<label for="save"><span for="save"></span>Save Vehicle</label>
											</span>
										</div>
									</div><!-- /.vehicle__cta -->
								</a>
							</div><!-- /.vehicle -->
							<div class="vehicle">
								<a href="">
									<div class="vehicle__image">
										<i class="icon / vehicle__img-count"><span>21</span></i>
										<img src="../assets/imgs/__temp/car-1.jpg" alt="">
									</div><!-- /.image -->
									<div class="vehicle_spec">
										<div class="cf / spec-row">
											<h3 class="vehicle__title">2012 Sportage EX 2.0 4WD</h3>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__colour">Charcoal grey with Black Leather</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__mileage">1,000km</span>
											<span class="vehicle__location">Niederlasung Frankfurt</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__phone">Tel: 069/2601010</span>
										</div>
									</div><!-- /.vehicle_spec -->
									<div class="vehicle__cta">
										<div class="cf / spec-row">
											<span class="vehicle__price">&euro;19,990</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__distance">1.2 km from postcode</span>
										</div>
										<div class="cf / spec-row">
											<span class="vehicle__save">
												<input type="checkbox" id="save" name="save" />
												<label for="save"><span for="save"></span>Save Vehicle</label>
											</span>
										</div>
									</div><!-- /.vehicle__cta -->
								</a>
							</div><!-- /.vehicle -->
						</div><!-- /.results .results--list-->
					</main>
				</div><!-- /.content -->
			</form>
		</div><!-- /.container -->
		<footer class="container">
			<div class="content">
				<p class="copyright">Copyright &copy; $year $dealer $country All Rights Reserved</p>
				<div class="links">
					<ul>
						<li><a href="#" title="Worldwide">Worldwide <i class="icon globe"></i></a></li>
					</ul>
				</div><!-- /.language -->
				<div class="social-media">
					<ul>
						<li><a href="#" title="Facebook"><i class="social-media__icons social-media__icons--fb"></i></a></li>
						<li><a href="#" title="Twitter"><i class="social-media__icons social-media__icons--tw"></i></a></li>
						<li><a href="#" title="YouTube"><i class="social-media__icons social-media__icons--yt"></i></a></li>
					</ul><!-- /.social-media__icons -->
				</div><!-- /.social-media -->
				<div class="footer--logo">
					<img src="/assets/imgs/footer-logo.png" alt="KIA - The Power To Surprise">
				</div><!-- /.footer--logo -->
			</div><!-- /.content -->
		</footer><!-- /.container -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="/assets/js/main.js"></script>
		<script src="/assets/js/plugins.js"></script>
	</body>
</html>