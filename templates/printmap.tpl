
<!DOCTYPE HTML>
<html>
<meta charset="utf-8">
<title>{if $dealer->dealername != ''}{$dealer->dealername}{else} Mercedes-Benz {/if} Directions</title>
<link rel="stylesheet" type="text/css" href="/css/layout.css"/>

{literal}
<style>
#headerprint {
width: 660px;
height: 89px;
background: url(/img/headerbkprint.gif) repeat-x #999999;
}
#map_canvas{
	clear:both;
	width:660px;
	height:360px;
}
.adp-directions{
	width: 660px;
}
</style>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
  var directionDisplay;
  var directionsService = new google.maps.DirectionsService();
  var map;

  function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
	var image = '';
{/literal}

var centermap = new google.maps.LatLng({$lat|strip_tags}, {$long|strip_tags}) ;
{literal}

    var myOptions = {
      zoom:10,
	  mapTypeControl: true,
	  mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
	  navigationControl: true,
	  navigationControlOptions: {
        style: google.maps.NavigationControlStyle.ZOOM_PAN,
        position: google.maps.ControlPosition.TOP_LEFT
    },
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: centermap
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById("directionsPanel"));

	var marker = new google.maps.Marker({
        position: centermap,
        map: map,
        icon: image
    });



  }

  function calcRoute() {
    var start = document.getElementById("start").value;//.replace(/[^0-9a-zA-Z ]/g,'');
	{/literal}
    var lat = parseFloat('{$lat|strip_tags}');
    var lng = parseFloat('{$long|strip_tags}');
    var end = new google.maps.LatLng(lat, lng);

	{literal}

    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.IMPERIAL
    };
    directionsService.route(request, function(response, status) {

      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      }
    });
  }

</script>

{/literal}

</head>

<body onload="initialize();calcRoute();" id="Form">
{assign var=addressFound value=false}
{foreach from=$dealer->departmentAddresses key=dept item=address name="addresses"}
{if $smarty.foreach.addresses.first}
<input type="hidden" id="end" value="{$address->latitude},{$address->longitude}" />
{/if}
{/foreach}

{php}
//security fix for postcode
$startValue = urldecode($this->get_template_vars('start'));
$pattern = "|^[0-9a-zA-Z ]+$|";
$startValue = preg_grep($pattern, array($startValue));
$startValue = implode("", $startValue);
{/php}
<input type="hidden" id="start" value="{php}echo $startValue;{/php}"/><input type="hidden" id="end" value="{$end}"/>

<input name="directions" type="hidden" value="{php}echo $startValue;{/php}" onClick="this.value=''" id="start" style="width:110px"/>



	<div style="margin-left:auto; margin-right:auto; width:660px">
         <div id="headerprint" class="clearfix">
              <div id="logo" class="left"><a href=""><img src="/img/mblogoprint.gif" alt="Mercedes-Benz  Dealername" border="0" /></a></div>
         </div>
         <div class="clearfix left">
           <a href="javascript: void(0)" onclick="window.print()"><img src="/img/hitlist/print.gif" width="148" height="21" border="0" /></a>
         </div>



         <div id="map_canvas"></div>

         <div id="directionsPanel" class="clearfix"></div>
    </div>
</body>
</html>
