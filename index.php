<!DOCTYPE html>
<!--
// ////////////////////////////////////////////////////////////////////////////
//
// Developed by Motortrak Ltd
//
// Address:   Motortrak Ltd
//            AC Court
//            High Street
//            Thames Ditton
//            KT7 0SR
//
// Software:  my.Motortrak
//
// Tel:       +44 (0)20 8335 2000
// E-Mail:    info@motortrak.com
//
// Copyright: Motortrak Ltd {'Y'|date}. All rights reserved.
//
// ////////////////////////////////////////////////////////////////////////////
-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no">
		<title>Kia</title>

		<meta name="application-name" content="{$application->applicationBuildMeta}"/>
        <meta name="keywords" content="{$meta->keywords}">
        <meta name="description" content="{$meta->description}">

        <meta http-equiv="cleartype" content="on">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0" />

		<link rel="shortcut icon" href="favicon.ico" />

		 <link rel="stylesheet" href="/assets/css/base.css">
		 <link rel="stylesheet" href="/assets/css/kia.css">
		 <link rel="stylesheet" href="/assets/css/magic.min.css">
		 <script src="/assets/js/modernizr.min.js"></script>
	</head>
	<body class="kia">
		<header class="container / cf">
			<div class="content">
				<div class="header--logo">
					<a href="/" title="Kia">
						<img src="/assets/imgs/kia-logo.png" alt="Kia">
					</a>
				</div>
				<h1 class="header--heading">Kia Used Cars - Region</h1>
				<nav class="nav--main">
					<ul>
						<li><a href="#" class="active" title="Search">Search</a></li>
						<li><a href="#" title="Kia Used Cars">Kia Used Cars</a></li>
					</ul>
				</nav>
			</div><!-- /.content -->
		</header>
		<div class="container">
			<form>
				<div class="cf / content / content-wrapper">
					<main class="cf">
						<div class="search / search--closed" id="side_panel">
							<div class="cf / search__header">
								<i class="icon / search__search-icon"></i>
								<h3 class="search__title">Advanced</h3>
								<i class="icon / search__close-fake-icon"></i>
								<i class="icon / search__close-icon"></i>
							</div><!-- /.search .search--open -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Mileage <i class="icon arrow"></i></span>
								</dt>
								<dd class="search__element__mileage">
									<select id="mileage" name="mileage">
										<option>Any (up to)</option>
										<option value="20000">Up to 20,000 km</option>
										<option value="40000">Up to 40,000 km</option>
										<option value="60000">Up to 60,000 km</option>
										<option value="80000">Up to 80,000 km</option>
										<option value="100000">Up to 100,000 km</option>
										<option value="120000">Up to 120,000 km</option>
									</select>
								</dd><!-- /.search__element__mileage -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Age <i class="icon arrow"></i></span>
								</dt>
								<dd class="cf / search__element__age">
									<select id="agefrom" name="age">
										<option>From</option>
									</select>
									<select id="ageto" name="age">
										<option>To</option>
									</select>
								</dd><!-- /.search__element__age -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Bodystyle <i class="icon arrow"></span></i>
								</dt>
								<dd class="cf / search__element__bodystyle">
									<div class="left">
										<div class="checkbox">
											<input type="checkbox" id="hatchback" name="hatchback" />
											<span for="hatchback"></span> <label>Hatchback</label>
										</div>
										<div class="checkbox">
											<input type="checkbox" id="estate" name="estate" />
											<span for="estate"></span> <label>Estate</label>
										</div>
										<div class="checkbox">
											<input type="checkbox" id="suv" name="suv" />
											<span for="mpv"></span> <label>SUV</label>
										</div>
									</div><!-- /.left -->
									<div class="right">
										<div class="checkbox">
											<input type="checkbox" id="saloon" name="saloon" />
											<span for="saloon"></span> <label>Saloon</label>
										</div>
										<div class="checkbox">
											<input type="checkbox" id="mpv" name="mpv" />
											<span for="mpv"></span> <label>MPV</label>
										</div>
									</div><!-- /.right -->
								</dd><!-- /.search__element__bodystyle -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Transmission <i class="icon arrow"></span></i>
								</dt>
								<dd class="search__element__transmission">
									<select id="transmission" name="transmission">
										<option value="">Any</option>
										<option value="">Manual</option>
										<option value="">Automatic</option>
										<option value="">Semi-Automatic</option>
									</select>
								</dd><!-- /.search__element__transmission -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Fuel Type <i class="icon arrow"></span></i>
								</dt>
								<dd class="search__element__transmission">
									<select id="transmission" name="transmission">
										<option value="">Any</option>
										<option value="">Petrol</option>
										<option value="">Diesel</option>
									</select>
								</dd><!-- /.search__element__transmission -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Colour <i class="icon arrow"></span></i>
								</dt>
								<dd class="cf / search__element__colour">
									<div class="checkbox">
										<div class="black">
											<input type="checkbox" id="black" value="black" name="black">
											<label for="black"><span for="black"></span></label>
										</div>
										<div class="white">
											<input type="checkbox" id="white" value="white" name="white">
											<label for="white"><span for="white"></span></label>
										</div>
										<div class="grey">
											<input type="checkbox" id="grey" value="grey" name="grey">
											<label for="grey"><span for="grey"></span></label>
										</div>
										<div class="brown">
											<input type="checkbox" id="brown" value="brown" name="brown">
											<label for="brown"><span for="brown"></span></label>
										</div>
										<div class="blue">
											<input type="checkbox" id="blue" value="blue" name="blue">
											<label for="blue"><span for="blue"></span></label>
										</div>
										<div class="green">
											<input type="checkbox" id="green" value="green" name="green">
											<label for="green"><span for="green"></span></label>
										</div>
										<div class="red">
											<input type="checkbox" id="red" value="red" name="red">
											<label for="red"><span for="red"></span></label>
										</div>
										<div class="yellow">
											<input type="checkbox" id="yellow" value="yellow" name="yellow">
											<label for="yellow"><span for="yellow"></span></label>
										</div>
									</div>
								</dd><!-- /.search__element__colour -->
							</dl><!-- /.search__element -->

							<dl class="search__element / is-hidden">
								<dt>
									<span class="search__element__title">Price <i class="icon arrow"></i></span>
								</dt>
								<dd class="cf / search__element__price">
									<select>
										<option value="">From</option>
									</select>
									<select>
										<option value="">To</option>
									</select>
								</dd><!-- /.search__element__price -->
							</dl><!-- /.search__element -->

							<div class="search__element / is-hidden">
								<div class="search__element__used">
									<div class="checkbox">
										<input type="checkbox" id="used" name="used" />
										<label for="used"><span for="used"></span>Kia Used Cars</label>
									</div>
	                            </div><!-- /.search__element__used -->
							</div><!-- /.search__element -->

							<div class="search__element / search__element--fixed / is-hidden">
								<div class="search__element__reset">
									<i class="icon / reset"></i><input type="reset">
								</div><!-- /.search__element__reset -->
							</div><!-- /.search__element -->
						</div><!-- /.search / .search--closed -->


						<div class="matrix search-is-closed / cf">
							<ul class="tabs">
								<li class="active" rel="all">All</li>
								<li rel="compact">Compact</li>
								<li rel="mid-size">Mid-size</li>
								<li rel="mpv-suv">MPV/SUV</li>
								<div class="is-disabled"></div>
							</ul>

							<div class="tab_container">
								<h3 class="d_active / tab_drawer_heading" rel="all">All</h3>
								<div id="all" class="tab_content / all">
									<div class="row / cf">
										<div class="quarter / col / model">
											<div class="is-disabled"></div>
											<a href="picanto-model-info.html" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/picanto.png" alt="Picanto" class="model__image">
											<input type="checkbox" id="picanto" name="picanto" />
											<span for="picanto"></span> <label>Picanto</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="rio-model-info.html" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/rio.png" alt="Rio" class="model__image">
											<input type="checkbox" id="rio" name="rio" />
											<span for="rio"></span> <label>Rio</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/soul.png" alt="Soul" class="model__image">
											<input type="checkbox" id="soul" name="soul" />
											<span for="soul"></span> <label>Soul</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/venga.png" alt="Venga" class="model__image">
											<input type="checkbox" id="venga" name="venga" />
											<span for="venga"></span> <label>Venga</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
									<div class="row / cf">
										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/ceed.png" alt="cee'd" class="model__image">
											<input type="checkbox" id="ceed" name="ceed" />
											<span for="ceed"></span> <label>cee'd</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/ceed-sw.png" alt="cee'd SW" class="model__image">
											<input type="checkbox" id="ceed-sw" name="ceed-sw" />
											<span for="ceed-sw"></span> <label>cee'd SW</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/pro-ceed.png" alt="pro_cee'd" class="model__image">
											<input type="checkbox" id="pro-ceed" name="pro-ceed" />
											<span for="pro-ceed"></span> <label>pro_cee'd</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/optima.png" alt="Optima" class="model__image">
											<input type="checkbox" id="optima" name="optima" />
											<span for="optima"></span> <label>Optima</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
									<div class="row / cf">
										<div class="quarter / col / model">
											<a rel="mpv-suv" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/carens.png" alt="Carens" class="model__image">
											<input type="checkbox" id="carens" name="carens" />
											<span for="carens"></span> <label>Carens</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/carnival.png" alt="Carnival" class="model__image">
											<input type="checkbox" id="carnival" name="carnival" />
											<span for="carnival"></span> <label>Carnival</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/sportage.png" alt="Sportage" class="model__image">
											<input type="checkbox" id="sportage" name="sportage" />
											<span for="sportage"></span> <label>Sportage</label>
										</div><!-- /.model -->

										<div class="quarter / col / model">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/sorento.png" alt="Sorento" class="model__image">
											<input type="checkbox" id="sorento" name="sorento" />
											<span for="sorento"></span> <label>Sorento</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
								</div><!-- /#all -->

								<h3 class="tab_drawer_heading" rel="compact">Compact</h3>
								<div id="compact" class="tab_content / compact">
									<div class="row / top / cf">
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/compact/picanto.png" alt="Picanto" class="model__image">
											<input type="checkbox" id="picanto2" name="picanto2" />
											<span for="picanto2"></span> <label>Picanto</label>
										</div><!-- /.model -->
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/compact/rio.png" alt="Rio" class="model__image">
											<input type="checkbox" id="rio2" name="rio2" />
											<span for="rio2"></span> <label>Rio</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
									<div class="row / bot / cf">
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/compact/soul.png" alt="Soul" class="model__image">
											<input type="checkbox" id="soul2" name="soul2" />
											<span for="soul2"></span> <label>Soul</label>
										</div><!-- /.model -->
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/compact/venga.png" alt="Venga" class="model__image">
											<input type="checkbox" id="venga2" name="venga2" />
											<span for="venga2"></span> <label>Venga</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
								</div><!-- #compact -->

								<h3 class="tab_drawer_heading" rel="mid-size">Mid-size</h3>
								<div id="mid-size" class="tab_content / mid-size">
									<div class="row / top / cf">
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mid-size/ceed.png" alt="cee'd" class="model__image">
											<input type="checkbox" id="ceed2" name="ceed2" />
											<span for="ceed2"></span> <label>cee'd</label>
										</div><!-- /.model -->
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mid-size/ceed-sw.png" alt="cee'd SW" class="model__image">
											<input type="checkbox" id="ceed-sw2" name="ceed-sw2" />
											<span for="ceed-sw2"></span> <label>cee'd SW</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
									<div class="row / bot / cf">
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mid-size/pro-ceed.png" alt="pro_cee'd" class="model__image">
											<input type="checkbox" id="pro-ceed2" name="pro-ceed2" />
											<span for="pro-ceed2"></span> <label>pro_cee'd</label>
										</div><!-- /.model -->
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mid-size/optima.png" alt="Optima" class="model__image">
											<input type="checkbox" id="optima2" name="optima2" />
											<span for="optima2"></span> <label>Optima</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
								</div><!-- #mid-size -->

								<h3 class="tab_drawer_heading" rel="mpv-suv">MPV/SUV</h3>
								<div id="mpv-suv" class="tab_content / mpv-suv">
									<div class="row / top / cf">
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mpv-suv/carens.png" alt="Carens" class="model__image">
											<input type="checkbox" id="carens2" name="carens2" />
											<span for="carens2"></span> <label>Carens</label>
										</div><!-- /.model -->
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mpv-suv/carnival.png" alt="Carnival" class="model__image">
											<input type="checkbox" id="carnival2" name="carnival2" />
											<span for="carnival2"></span> <label>Carnival</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
									<div class="row / bot / cf">
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mpv-suv/sportage.png" alt="Sportage" class="model__image">
											<input type="checkbox" id="sportage2" name="sportage2" />
											<span for="sportage2"></span> <label>Sportage</label>
										</div><!-- /.model -->
										<div class="half / col / model model--large">
											<a href="#" class="model__optionslink">
												<i class="icon model__optionslink--icon"></i><span class="arrow / model__optionslink--label">Model Options</span></a>
											<img src="../assets/imgs/matrix/mpv-suv/sorento.png" alt="Sorento" class="model__image">
											<input type="checkbox" id="sorento2" name="sorento2" />
											<span for="sorento2"></span> <label>Sorento</label>
										</div><!-- /.model -->
									</div><!-- /.row -->
								</div><!-- #mpv-suv -->
							</div><!-- .tab_container -->
						</div><!-- /.matrix -->
					</main>
					<aside>
						<h1 class="main-heading">Kia Used Cars</h1>
						<h2 class="sub-heading">3 to 6 year warranty</h2>
						<hr>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae at, similique. Sequi eligendi, similique sunt quod, earum hic distinctio laudantium animi quidem qui, iure voluptatibus modi doloribus eius sapiente quos.</p>
						<p>Distinctio, ratione asperiores in laboriosam. Dolorum nostrum, fugiat earum soluta voluptatibus voluptates ipsa quod! Perferendis modi perspiciatis ex fuga in odit asperiores nobis. Ipsam facere suscipit deserunt, cum sint enim.</p>
					</aside>
				</div><!-- /.content -->
				<div class="cf / content / basic-search">
					<div class="cf / float-left / location">
						<label for="postcode-basic" class="location__heading">Enter Location</label>
						<input type="text" id="postcode-basic" name="postcode-basic" class="search__location / float-left" placeholder="Postcode" required>
						<select class="search__distance / float-left">
							<option>Any Distance</option>
						</select>
					</div>
					<div class="cf / float-right / search-results">
						<div class="search__results">
							<span class="search__results--number">50</span><sup>+</sup> matches
						</div><!-- /.search__results -->
						<div class="search__button">
							<button class="button button--red" type="submit">Search</button>
						</div><!-- /.search__button -->
					</div><!-- /.float-right -->
				</div><!-- /.content -->
			</form>
		</div><!-- /.container -->
		<footer class="container">
			<div class="content">
				<p class="copyright">Copyright &copy; $year $dealer $country All Rights Reserved</p>
				<div class="links">
					<ul>
						<li><a href="#" title="Worldwide">Worldwide <i class="icon globe"></i></a></li>
					</ul>
				</div><!-- /.language -->
				<div class="social-media">
					<ul>
						<li><a href="#" title="Facebook"><i class="social-media__icons social-media__icons--fb"></i></a></li>
						<li><a href="#" title="Twitter"><i class="social-media__icons social-media__icons--tw"></i></a></li>
						<li><a href="#" title="YouTube"><i class="social-media__icons social-media__icons--yt"></i></a></li>
					</ul><!-- /.social-media__icons -->
				</div><!-- /.social-media -->
				<div class="footer--logo">
					<img src="/assets/imgs/footer-logo.png" alt="KIA - The Power To Surprise">
				</div><!-- /.footer--logo -->
			</div><!-- /.content -->
		</footer><!-- /.container -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="/assets/js/jquery.pjax.js"></script>
		<script src="/assets/js/main.js"></script>
		<script src="/assets/js/plugins.js"></script>
	</body>
</html>