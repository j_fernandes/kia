<!DOCTYPE html>
<!--
// ////////////////////////////////////////////////////////////////////////////
//
// Developed by Motortrak Ltd
//
// Address:   Motortrak Ltd
//            AC Court
//            High Street
//            Thames Ditton
//            KT7 0SR
//
// Software:  my.Motortrak
//
// Tel:       +44 (0)20 8335 2000
// E-Mail:    info@motortrak.com
//
// Copyright: Motortrak Ltd {'Y'|date}. All rights reserved.
//
// ////////////////////////////////////////////////////////////////////////////
-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no">
		<title>Kia</title>

		<meta name="application-name" content="{$application->applicationBuildMeta}"/>
        <meta name="keywords" content="{$meta->keywords}">
        <meta name="description" content="{$meta->description}">

        <meta http-equiv="cleartype" content="on">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0" />

		<link rel="shortcut icon" href="{$domainname_uri}/tomhartleyjnr/img/favicon.ico" />

		<link rel="stylesheet" href="/assets/css/base.css">
		<link rel="stylesheet" href="/assets/css/kia.css">
		<link rel="stylesheet" href="/assets/css/plugins/flexslider.css">
		<link rel="stylesheet" href="/assets/css/plugins/magnific-popup.css">

	</head>
	<body class="kia">
		<header class="container / cf">
			<div class="content">
				<div class="header--logo">
					<a href="/" title="Kia">
						<img src="/assets/imgs/kia-logo.png" alt="Kia">
					</a>
				</div>
				<h1 class="header--heading">Kia Used Cars - Region</h1>
				<nav class="nav--main">
					<ul>
						<li><a href="#" class="active" title="Search">Search</a></li>
						<li><a href="#" title="Kia Used Cars">Kia Used Cars</a></li>
					</ul>
				</nav>
			</div><!-- /.content -->
		</header>
		<div class="cf / container / details">
			<main class="cf / content">
				<section class="cf / toolbar">
					<ul class="pager">
						<li class="back">
							<a href="{$returnurl}"><i class="icon back"></i>Back to search</a>
						</li>
						<div class="float-right">
							<li class="prev">
								<a href="{$vehicleobj->getvehicleurl($items.prevcar)}"><i class="icon prev {if $items.prevcar == '0'}is-disabled{/if}"></i>Previous Vehicle</a>
							</li>
							<li class="next">
								<a href="{$vehicleobj->getvehicleurl($items.nextcar)}"><i class="icon next {if $items.nextcar == '0'}is-disabled{/if}"></i> Next Vehicle</a>
							</li>
						</div>
					</ul>
				</section><!-- /.toolbar -->
				<section class="cf / vehicle">
					<header class="cf">
						<h1 class="vehicle__title">
							<!-- {$items.YEAR|lower} {$items.MODEL} {$items.VARIANT} {translate}in{/translate} {$items.TOWN} {translate}for sale{/translate} -->
							2012 Sportage 1.6 GDI Spirit <span class="vehicle__title--location">in Frankfurt for sale</span>
						</h1>
						<div class="phone">
							<i class="icon phone__icon"></i><span class="phone__number">069/2601010</span>
						</div>
					</header>
					<section class="vehicle__details">
						<div class="cf / spec-row">
							<span class="vehicle__price">
							&euro;19.000
	<!-- 							{if $items.VID != ''}
									&euro;{$items.PRICE}
								{else}
									{translate}Sold{/translate}
								{/if} -->
							</span>
						</div>
						<div class="cf / spec-row">
							<span class="vehicle__approved">
								<img src="../assets/imgs/approved-logo.gif">
							</span>
						</div>

						<table class="cf / vehicle__spec">
							<tbody>
								<tr>
									<th scope="row">
										<!-- {translate}Exterior:{/translate} -->
										Exterior:
									</th>
									<td>
<!-- 										{if $items.COLOUR}
											{$items.COLOUR}
										{else}
											&#8211;
										{/if} -->
										Black
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Interior:{/translate} -->
										Interior:
									</th>
									<td>
<!-- 										{if $items.TRIM}
											{$items.TRIM}
										{else}
											&#8211;
										{/if} -->
										Leather
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Mileage:{/translate} -->
										Mileage:
									</th>
									<td>
<!-- 										{if $items.MILEAGE}
											{$items.MILEAGE}
										{else}
											&#8211;
										{/if} -->
										1,000 km
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Transmission:{/translate} -->
										Transmission:
									</th>
									<td>
<!-- 										{if $items.TRANSMISSION}
											{$items.TRANSMISSION}
										{else}
											&#8211;
										{/if} -->
										Manual
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Fuel:{/translate} -->
										Fuel:
									</th>
									<td>
<!-- 										{if $items.ENGINE}
											{$items.ENGINE}
										{else}
											&#8211;
										{/if} -->
										Petrol
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Capacity:{/translate} -->
										Capacity:
									</th>
									<td>
<!-- 										{if $items.CAPACITY}
											{$items.CAPACITY}
										{else}
											&#8211;
										{/if} -->
										1591 ccm
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}CO2:{/translate} -->
										CO2:
									</th>
									<td>
<!-- 										{if $items.DESCRIPTOR_MVRIS_CO2}
											{$items.DESCRIPTOR_MVRIS_CO2}
										{else}
											&#8211;
										{/if} -->
										158 g/km
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}CO2 Efficiency Band:{/translate} -->
										CO2 Efficiency Band:
									</th>
									<td>
										D
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Bodystyle:{/translate} -->
										Bodystyle:
									</th>
									<td>
<!-- 										{if $items.BODY}
											{$items.BODY}
										{else}
											&#8211;
										{/if} -->
										Off-road 4WD
									</td>
								</tr>
								<tr>
									<th scope="row">
										<!-- {translate}Location:{/translate} -->
										Location:
									</th>
									<td>
										Niederlassung Frankfurt
									</td>
								</tr>
							</tbody>
						</table>
					</section><!-- /.vehicle__details -->
					<section class="cf / vehicle__gallery">
						<div id="slider" class="slider / flexslider">
						<i class="icon / vehicle__img-count"><span>21</span></i>
						<a href=""><i class="icon / vehicle__zoom-icon"></i></a>
							<ul class="slides">
								<li><img src="../assets/imgs/__temp/car-1.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-2.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-3.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-1.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-2.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-3.jpg"></li>
							</ul><!-- /.slides -->
							<!-- <a href=""><i class="icon / vehicle__play-icon"></i></a> -->
						</div><!-- /#slider -->
						<div id="carousel" class="carousel / flexslider">
							<ul class="slides">
								<li><img src="../assets/imgs/__temp/car-1.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-2.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-3.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-1.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-2.jpg"></li>
								<li><img src="../assets/imgs/__temp/car-3.jpg"></li>
							</ul><!-- /.slides -->
						</div><!-- /#carousel -->
					</section><!-- /.gallery -->
					<section class="cf / cta">
<!-- 					<a class="button button--red / contact">{translate}Contact Dealer{/translate}</a>
						<a class="button button--red / testdrive">{translate}Test Drive Request{/translate}</a>
						<a class="button button--red / finance">{translate}Finance Quote{/translate}</a>
-->
						<a href="contact.php" class="ajax-popup button button--red / contact2">Contact Dealer</a>
						<a class="button button--red / testdrive">Test Drive Request</a>
						<a class="button button--red / finance">Finance Quote</a>
						<div class="cf / spec-row">
							<span class="vehicle__save">
								<div class="checkbox">
									<input type="checkbox" id="save" name="save" />
									<!-- <label for="save"><span for="save"></span>{translate}Save Vehicle{/translate}</label> -->
									<label for="save"><span for="save"></span>Save Vehicle</label>
								</div>
							</span>
						</div>
						<div class="cf / spec-row">
							<span class="vehicle__print">
								<a href="/detail?template=/printdetails/print_4x3&amp;vid={$items.VID}" target="_blank"><i class="icon arrowright"></i>Print</a>
							</span>
						</div>
						<div class="cf / spec-row">
							<span class="vehicle__emailfriend">
								<!-- <a onclick="window.open('/detail?template=/email/emailafriend&amp;vid={$items.VID}', 'SendTo', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=400,left = 640,top = 325')"><i class="icon arrowright"></i>{translate}Email a friend{/translate}</a> -->
								<a onclick="window.open('/detail?template=/email/emailafriend&amp;vid={$items.VID}', 'SendTo', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=400,left = 640,top = 325')"><i class="icon arrowright"></i>Email a friend</a>
							</span>
						</div>

						<section class="cf / social-media">
							<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2F{$items.CPUBLICURL|replace:'http://':''|replace:'/':'%2F'}&amp;width=48&amp;layout=button&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:48px; height:20px;" allowTransparency="true"></iframe>

							<!-- <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">{translate}Tweet{/translate}</a> -->
							<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>


							<!-- {literal}<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>{/literal} -->
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
							<div class="g-plusone" data-size="medium" data-annotation="none"></div><!-- /.g-plusone -->
							<!-- {literal}<script type="text/javascript">(function() {var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/platform.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })();</script>{/literal} -->
							<script type="text/javascript">(function() {var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/platform.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })();</script>
						</section><!-- /.social-media -->
						<section class="cf / qr-code">
							<img src="../assets/imgs/__temp/qr-code.gif">
						</section><!-- /.qr-code -->
					</section><!-- /.cta -->

					<section class="cf / specifications">
						<h2 class="specifications__heading">Sportage 1.6 GDi Spirit Specifications</h2>
						<ul class="tabs">
<!-- 							<li class="active" rel="standard">{translate}Standard Features{/translate}</li>
							<li rel="additional">{translate}Additional Features{/translate}</li>
							<li rel="comments">{translate}Dealer Comments{/translate}</li>
							<li rel="finance">{translate}Finance Information{/translate}</li>
							<li rel="insurance">{translate}Insurance Information{/translate}</li> -->
							<li class="active" rel="standard">Standard Features</li>
							<li rel="additional">Additional Features</li>
							<li rel="comments">Dealer Comments</li>
							<li rel="finance">Finance Information</li>
							<li rel="insurance">Insurance Information</li>
						</ul>
						<div class="tab_container">
							<h3 class="d_active / tab_drawer_heading" rel="standard">Standard Features</h3>
							<div id="standard" class="cf / tab_content">
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
							</div><!-- #standard -->

							<h3 class="d_active / tab_drawer_heading" rel="addictional">Additional Features</h3>
							<div id="additional" class="cf / tab_content">
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
								<ul>
									<li>Alarm</li>
									<li>Cruise Control</li>
									<li>Curtain Airbags</li>
									<li>Emergency stop signalling</li>
									<li>Front fog lights</li>
									<li>High mounted rear brake lights</li>
									<li>Privacy glass</li>
									<li>Rain sensor windscreen wipers</li>
									<li>Reverse parking aid</li>
									<li>Side airbags</li>
								</ul>
							</div><!-- #addictional -->

							<h3 class="d_active / tab_drawer_heading" rel="comments">Dealer Comments</h3>
							<div id="comments" class="cf / tab_content">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore possimus blanditiis, eos expedita distinctio ipsam fuga deleniti soluta modi voluptatem laboriosam esse, libero, facere molestiae vitae! Nemo ipsum quia omnis?</p>
								<p>Quisquam blanditiis atque, officiis sunt incidunt, possimus quaerat adipisci. Reprehenderit quae praesentium, suscipit, totam rerum aspernatur neque aut excepturi deserunt tenetur quaerat omnis. Quae culpa quis fuga magnam. Porro, nihil?</p>
							</div><!-- #comments -->

							<h3 class="d_active / tab_drawer_heading" rel="finance">Finance Information</h3>
							<div id="finance" class="cf / tab_content">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore possimus blanditiis, eos expedita distinctio ipsam fuga deleniti soluta modi voluptatem laboriosam esse, libero, facere molestiae vitae! Nemo ipsum quia omnis?</p>
								<p>Quisquam blanditiis atque, officiis sunt incidunt, possimus quaerat adipisci. Reprehenderit quae praesentium, suscipit, totam rerum aspernatur neque aut excepturi deserunt tenetur quaerat omnis. Quae culpa quis fuga magnam. Porro, nihil?</p>							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore possimus blanditiis, eos expedita distinctio ipsam fuga deleniti soluta modi voluptatem laboriosam esse, libero, facere molestiae vitae! Nemo ipsum quia omnis?</p>
								<p>Quisquam blanditiis atque, officiis sunt incidunt, possimus quaerat adipisci. Reprehenderit quae praesentium, suscipit, totam rerum aspernatur neque aut excepturi deserunt tenetur quaerat omnis. Quae culpa quis fuga magnam. Porro, nihil?</p>
							</div><!-- #finance -->

							<h3 class="d_active / tab_drawer_heading" rel="insurance">Insurance Features</h3>
							<div id="insurance" class="cf / tab_content">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore possimus blanditiis, eos expedita distinctio ipsam fuga deleniti soluta modi voluptatem laboriosam esse, libero, facere molestiae vitae! Nemo ipsum quia omnis?</p>
								<p>Quisquam blanditiis atque, officiis sunt incidunt, possimus quaerat adipisci. Reprehenderit quae praesentium, suscipit, totam rerum aspernatur neque aut excepturi deserunt tenetur quaerat omnis. Quae culpa quis fuga magnam. Porro, nihil?</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore possimus blanditiis, eos expedita distinctio ipsam fuga deleniti soluta modi voluptatem laboriosam esse, libero, facere molestiae vitae! Nemo ipsum quia omnis?</p>
								<p>Quisquam blanditiis atque, officiis sunt incidunt, possimus quaerat adipisci. Reprehenderit quae praesentium, suscipit, totam rerum aspernatur neque aut excepturi deserunt tenetur quaerat omnis. Quae culpa quis fuga magnam. Porro, nihil?</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore possimus blanditiis, eos expedita distinctio ipsam fuga deleniti soluta modi voluptatem laboriosam esse, libero, facere molestiae vitae! Nemo ipsum quia omnis?</p>
								<p>Quisquam blanditiis atque, officiis sunt incidunt, possimus quaerat adipisci. Reprehenderit quae praesentium, suscipit, totam rerum aspernatur neque aut excepturi deserunt tenetur quaerat omnis. Quae culpa quis fuga magnam. Porro, nihil?</p>
							</div><!-- #insurance -->
						</div><!-- /.tab_container -->
					</section><!-- /.specifications -->

					<section class="cf / dealer-info">
						<div class="dealer-info__details">
							<!-- <h3 class="dealer-info__name">{$items.DEALERNAME}</h3> -->
							<h3 class="dealer-info__name">Kia Motors Deutschland GmbH Niederlassung Frankfurt</h3>
							<address class="vcard">
								<!-- <div class="adr">
									<div class="street-address">{$items.ADD1}</div>
									{if $items.ADD2}
										<div class="street-address">{$items.ADD2}</div>
									{/if}
									<div class="locality">{$address->town}</div>
									<div>{$items.ADD3}</div>
									<div class="postal-code">{$items.POSTCODE}</div>
									<div class="country-name">{$items.COUNTRY}</div>
									{assign var=addressFound value=true}
									<div class="geo">GEO:
										<span class="latitude">{$items.YCOORD|replace:'-.':'-0.'}</span>,
										<span class="longitude">{$items.XCOORD|replace:'-.':'-0.'}</span>
									</div>
								</div> --><!-- /.adr -->
								<!-- <div class="tel">Tel: {$items.PHONE}</div> -->
								<div class="adr">
									<div class="street-address">Mainzer Landstraße 164</div>
									<div class="postal-code">60327</div>
									<div class="locality">Frankfurt am Main</div>
									<div class="country-name">Germany</div>

									<div class="geo">GEO:
										<span class="latitude">52.8382</span>,
										<span class="longitude">-2.327815</span>
									</div>
								</div><!-- /.adr -->
								<div class="tel">Tel: <span class="tel__number">069/2601010</span></div>
							</address>
						</div><!-- /.dealer-info__details -->
						<div class="dealer-info__map-directions">
							<div id="dealer-info__map" class="dealer-info__map"></div><!-- /.dealer-info__map -->
							<form name="direction-form" class="dealer-info__directions" onSubmit="window.open('/printmap.php?start='+(document.getElementById('findDealerInputLocation').value), 'directions', 'width=560, \ height=600, \ directories=no, \ location=no, \ menubar=no, \ resizable=no, \   scrollbars=1, \   status=no, \   toolbar=no');   return false;">
								<input type="hidden" class="start" value="" />
								<input type="text" id="findDealerInputLocation" name="directions" placeholder="Enter Postcode for directions" required>
									<button type="submit" class="go"><span>Go</span></button>
								</form>
						</div><!-- /.dealer-info__map-directions -->
						<div class="dealer-info__sitelink">
							<!-- <a class="button button--grey" href="#">{translate}View Dealer Site{translate/}</a> -->
							<a class="button button--grey" href="#">View Dealer Site</a>
						</div><!-- /.dealer-info__sitelink -->
					</section><!-- /.dealer-info -->
					<section class="cf / similar-vehicles">
						<h3>Similar Vehicles to Sportage 1.6 GDi Spirit</h3>
						<div class="flexslider">
							<ul class="slides">
								<li>
									<i class="icon / vehicle__img-count"><span>21</span></i>
									<img src="../assets/imgs/__temp/car-1.jpg" />
									<span class="similar-vehicles__title">Sportage 1.7 CRDi Spirit</span>
									<span class="similar-vehicles__price">&euro;35,495</span>
								</li>
								<li>
									<i class="icon / vehicle__img-count"><span>21</span></i>
									<img src="../assets/imgs/__temp/car-2.jpg" />
									<span class="similar-vehicles__title">Sportage 1.7 CRDi Spirit</span>
									<span class="similar-vehicles__price">&euro;35,495</span>
								</li>
								<li>
									<i class="icon / vehicle__img-count"><span>21</span></i>
									<img src="../assets/imgs/__temp/car-3.jpg" />
									<span class="similar-vehicles__title">Sportage 1.7 CRDi Spirit</span>
									<span class="similar-vehicles__price">&euro;35,495</span>
								</li>
								<li>
									<i class="icon / vehicle__img-count"><span>21</span></i>
									<img src="../assets/imgs/__temp/car-1.jpg" />
									<span class="similar-vehicles__title">Sportage 1.7 CRDi Spirit</span>
									<span class="similar-vehicles__price">&euro;35,495</span>
								</li>
								<li>
									<i class="icon / vehicle__img-count"><span>21</span></i>
									<img src="../assets/imgs/__temp/car-2.jpg" />
									<span class="similar-vehicles__title">Sportage 1.7 CRDi Spirit</span>
									<span class="similar-vehicles__price">&euro;35,495</span>
								</li>
								<li>
									<i class="icon / vehicle__img-count"><span>21</span></i>
									<img src="../assets/imgs/__temp/car-3.jpg" />
									<span class="similar-vehicles__title">Sportage 1.7 CRDi Spirit</span>
									<span class="similar-vehicles__price">&euro;35,495</span>
								</li>
							</ul>
						</div>
					</section><!-- /.similar-vehicles -->
				</section><!-- /.vehicle -->
			</main><!-- /.content -->
		</div><!-- /.container -->
		<footer class="cf / container">
			<div class="content">
				<p class="copyright">Copyright &copy; $year $dealer $country All Rights Reserved</p>
				<div class="links">
					<ul>
						<li><a href="#" title="Worldwide">Worldwide <i class="icon globe"></i></a></li>
					</ul>
				</div><!-- /.language -->
				<div class="social-media">
					<ul>
						<li><a href="#" title="Facebook"><i class="social-media__icons social-media__icons--fb"></i></a></li>
						<li><a href="#" title="Twitter"><i class="social-media__icons social-media__icons--tw"></i></a></li>
						<li><a href="#" title="YouTube"><i class="social-media__icons social-media__icons--yt"></i></a></li>
					</ul><!-- /.social-media__icons -->
				</div><!-- /.social-media -->
				<div class="footer--logo">
					<img src="/assets/imgs/footer-logo.png" alt="KIA - The Power To Surprise">
				</div><!-- /.footer--logo -->
			</div><!-- /.content -->
		</footer><!-- /.container -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="/assets/js/main.js"></script>
		<script src="/assets/js/plugins.js"></script>
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script>
			function init_map(){
				var image = '/assets/imgs/map-marker.png';
				var myOptions = {
					zoom:10,
					center: new google.maps.LatLng(52.8382,-2.327815),
					mapTypeId: google.maps.MapTypeId.ROADMAP};
					map = new google.maps.Map(document.getElementById("dealer-info__map"), myOptions);
					marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(52.8382, -2.327815),animation: google.maps.Animation.DROP,icon: image});
					// marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(52.8382, -2.327815)});
					//infowindow = new google.maps.InfoWindow({content:"<b>The Breslin</b><br/>2880 Broadway<br/> New York" });
					google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});
					//infowindow.open(map,marker);
					}google.maps.event.addDomListener(window, 'load', init_map);
		</script>

		<script>
			// var CookieClientName = 'KIA{$thispage}';
			// var VID = '{$items.VID}';

			// function initializeDetails() {
			// 	var image = '/assets/imgs/map-marker.png';
			// 	var mapOptions = {
			// 		// center: new google.maps.LatLng({$dealer->address->latitude}, {$dealer->address->longitude}),
			// 		center: new google.maps.LatLng(52.8382, -2.327815),
			// 		zoom: 10,
			// 		mapTypeId: google.maps.MapTypeId.ROADMAP,
			// 	};
			// 	var map = new google.maps.Map(document.getElementById('dealer-info__map'),
			// 		mapOptions);
			// 	var marker = new google.maps.Marker({
			// 		// position:  new google.maps.LatLng({$dealer->address->latitude}, {$dealer->address->longitude}),
			// 		position: new google.maps.LatLng(52.8382, -2.327815),
			// 		map: map,
			// 		animation: google.maps.Animation.DROP,
			// 		icon: image
			// 	});
			// }
			// {literal}
			// 	$(function(){
			// 		initializeDetails();activeShortlist();
			// 		carSimilar.init({containerID: 'similarVehicles', vin: '{/literal}{$items.REG}{literal}', searchtype: '{/literal}{$thispage}{literal}'});
			// 	});
			// {/literal}
		</script>
	</body>
</html>